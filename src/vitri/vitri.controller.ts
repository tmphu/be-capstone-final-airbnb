import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
} from '@nestjs/common';
import { ViTriService } from './vitri.service';
import { Response } from 'express';
import {
  ApiBody,
  ApiConsumes,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { ViTriSwaggerDto } from './dto/vitri.dto';
import {
  Delete,
  Param,
  Put,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common/decorators';
import { FileUploadDto } from 'src/fileupload.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

@ApiTags('ViTri')
@Controller('vi-tri')
export class ViTriController {
  constructor(private readonly viTriService: ViTriService) {}

  // Lay toan bo danh sach vi tri
  @Get('/')
  async getViTri(@Res() res: Response): Promise<Response> {
    try {
      const data = await this.viTriService.getViTri();

      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Tao moi vi tri
  @Post('/')
  async createViTri(
    @Res() res: Response,
    @Body() body: ViTriSwaggerDto,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.viTriService.createViTri(token, body);
      return res.status(201).json({
        message: 'Success',
        statusCode: 201,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Lay vi tri theo id
  @Get('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async getViTriById(
    @Res() res: Response,
    @Param('id') id: string,
  ): Promise<Response> {
    try {
      const data = await this.viTriService.getViTriById(id);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Cap nhat vi tri
  @Put('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async updateViTri(
    @Res() res: Response,
    @Body() body: ViTriSwaggerDto,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.viTriService.updateViTri(token, id, body);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Xoa vi tri
  @Delete('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async deleteViTri(
    @Res() res: Response,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.viTriService.deleteViTri(token, id);
      return res.status(200).json({
        message: 'Vi tri da duoc xoa thanh cong',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Upload hinh anh vi tri
  @Post('/upload-hinh-vitri')
  @ApiQuery({ name: 'id', required: true, type: Number })
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.cwd() + '/public/img',
        filename: (req, file, cb) =>
          cb(null, Date.now() + '_' + file.originalname),
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    description: 'Upload image',
    type: FileUploadDto,
  })
  async uploadHinhAnhViTri(
    @Res() res: Response,
    @Query('id') id: string,
    @Headers('userToken') token: string,
    @UploadedFile() file: Express.Multer.File,
  ): Promise<Response> {
    try {
      const viTri = await this.viTriService.getViTriById(id);
      if (!viTri) {
        throw new HttpException('Vi tri khong tim thay', HttpStatus.NOT_FOUND);
      }

      // Save the URL of the uploaded image to the 'hinh_anh' field of the 'vi_tri' record
      const imagePath = `/img/${file.filename}`;
      viTri.hinh_anh = imagePath;
      await this.viTriService.updateViTri(token, id, viTri);

      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: viTri,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }
}
