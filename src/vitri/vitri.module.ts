import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { ViTriController } from './vitri.controller';
import { ViTriService } from './vitri.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [ViTriController],
  providers: [ViTriService, AuthService],
})
export class VitriModule {}
