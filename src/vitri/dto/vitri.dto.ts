import { ApiProperty } from '@nestjs/swagger/dist';

interface ViTriDto {
  id: number;
  ten_vi_tri: string;
  tinh_thanh: string;
  quoc_gia: string;
  hinh_anh: string;
}

class ViTriSwaggerDto {
  @ApiProperty({
    example: 'Bai bien',
    description: 'ten_vi_tri',
    type: String,
  })
  ten_vi_tri: string;

  @ApiProperty({
    example: 'Nha Trang',
    description: 'tinh_thanh',
    type: String,
  })
  tinh_thanh: string;

  @ApiProperty({
    example: 'Viet Nam',
    description: 'quoc_gia',
    type: String,
  })
  quoc_gia: string;

  @ApiProperty({
    example: 'https://example.com/image.jpg',
    description: 'hinh_anh',
    type: String,
  })
  hinh_anh: string;
}

export { ViTriDto, ViTriSwaggerDto };
