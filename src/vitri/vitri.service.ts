import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { ViTriDto, ViTriSwaggerDto } from './dto/vitri.dto';

@Injectable()
export class ViTriService {
  constructor(private readonly authService: AuthService) {}
  private prisma = new PrismaClient();

  // Lay toan bo danh sach vi tri
  async getViTri(): Promise<ViTriDto[]> {
    const data = await this.prisma.vi_tri.findMany();
    return data;
  }

  // Lay vi tri theo id
  async getViTriById(idParam: string): Promise<ViTriDto> {
    const id = parseInt(idParam);
    const data = await this.prisma.vi_tri.findUnique({
      where: {
        id: id,
      },
    });
    return data;
  }

  // Tao moi vi tri
  async createViTri(token: string, viTri: ViTriSwaggerDto): Promise<ViTriDto> {
    const isValidToken = await this.authService.validateToken(token);
    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.vi_tri.create({
      data: viTri,
    });
    return data;
  }

  // Cap nhat vi tri
  async updateViTri(
    token: string,
    idParam: string,
    viTri: ViTriSwaggerDto,
  ): Promise<ViTriDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.vi_tri.update({
      where: {
        id: id,
      },
      data: viTri,
    });
    return data;
  }

  // Xoa vi tri
  async deleteViTri(token: string, idParam: string): Promise<ViTriDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.vi_tri.delete({
      where: {
        id: id,
      },
    });
    return data;
  }
}
