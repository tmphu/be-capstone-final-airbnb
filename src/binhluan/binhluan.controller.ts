import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
} from '@nestjs/common';
import { BinhLuanService } from './binhluan.service';
import { Response } from 'express';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { BinhLuanSwaggerDto } from './dto/binhluan.dto';
import { Delete, Param, Put } from '@nestjs/common/decorators';

@ApiTags('BinhLuan')
@Controller('binh-luan')
export class BinhLuanController {
  constructor(private readonly binhLuanService: BinhLuanService) {}

  // Lay toan bo danh sach binh luan
  @Get('/')
  async getBinhLuan(@Res() res: Response): Promise<Response> {
    try {
      const data = await this.binhLuanService.getBinhLuan();

      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Tao moi binh luan
  @Post('/')
  async createBinhLuan(
    @Res() res: Response,
    @Body() body: BinhLuanSwaggerDto,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.binhLuanService.createBinhLuan(token, body);
      return res.status(201).json({
        message: 'Success',
        statusCode: 201,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Lay binh luan theo id
  @Get('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async getBinhLuanById(
    @Res() res: Response,
    @Param('id') id: string,
  ): Promise<Response> {
    try {
      const data = await this.binhLuanService.getBinhLuanById(id);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Lay binh luan theo phong
  @Get('/lay-binh-luan-theo-phong/:phongId')
  @ApiParam({ name: 'phongId', required: true, type: Number })
  async getBinhLuanByPhongId(
    @Res() res: Response,
    @Param('phongId') id: string,
  ) {
    try {
      const data = await this.binhLuanService.getBinhLuanByPhongId(id);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Cap nhat binh luan
  @Put('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async updateBinhLuan(
    @Res() res: Response,
    @Body() body: BinhLuanSwaggerDto,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.binhLuanService.updateBinhLuan(token, id, body);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Xoa binh luan
  @Delete('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async deleteBinhLuan(
    @Res() res: Response,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.binhLuanService.deleteBinhLuan(token, id);
      return res.status(200).json({
        message: 'Binh luan da duoc xoa thanh cong',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }
}
