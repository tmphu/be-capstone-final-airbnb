import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { BinhLuanController } from './binhluan.controller';
import { BinhLuanService } from './binhluan.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [BinhLuanController],
  providers: [BinhLuanService, AuthService],
})
export class BinhluanModule {}
