import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { BinhLuanDto, BinhLuanSwaggerDto } from './dto/binhluan.dto';

@Injectable()
export class BinhLuanService {
  constructor(private readonly authService: AuthService) {}
  private prisma = new PrismaClient();

  // Lay toan bo danh sach binh luan
  async getBinhLuan(): Promise<BinhLuanDto[]> {
    const data = await this.prisma.binh_luan.findMany();
    return data;
  }

  // Lay binh luan theo id
  async getBinhLuanById(idParam: string): Promise<BinhLuanDto> {
    const id = parseInt(idParam);
    const data = await this.prisma.binh_luan.findUnique({
      where: {
        id: id,
      },
    });
    return data;
  }

  // Lay binh luan theo phong
  async getBinhLuanByPhongId(idParam: string): Promise<BinhLuanDto[]> {
    const id = parseInt(idParam);
    const data = await this.prisma.binh_luan.findMany({
      where: {
        ma_phong: id,
      },
      include: {
        nguoi_dung: {
          select: {
            name: true,
            hinh_anh: true,
          },
        },
      },
    });
    return data;
  }

  // Tao moi binh luan
  async createBinhLuan(
    token: string,
    binhLuan: BinhLuanSwaggerDto,
  ): Promise<BinhLuanDto> {
    const isValidToken = await this.authService.validateToken(token);
    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const isValidNguoiBinhLuan = await this.prisma.nguoi_dung.findUnique({
      where: {
        id: binhLuan.ma_nguoi_binh_luan,
      },
    });
    if (!isValidNguoiBinhLuan) {
      throw new Error('ID nguoi dung khong ton tai');
    }

    const isValidPhong = await this.prisma.phong.findUnique({
      where: {
        id: binhLuan.ma_phong,
      },
    });
    if (!isValidPhong) {
      throw new Error('ID phong khong ton tai');
    }

    binhLuan.ngay_binh_luan = new Date(binhLuan.ngay_binh_luan);
    binhLuan.sao_binh_luan = Math.floor(binhLuan.sao_binh_luan);
    const data = await this.prisma.binh_luan.create({
      data: binhLuan,
    });
    return data;
  }

  // Cap nhat binh luan
  async updateBinhLuan(
    token: string,
    idParam: string,
    binhLuan: BinhLuanSwaggerDto,
  ): Promise<BinhLuanDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const isValidNguoiBinhLuan = await this.prisma.nguoi_dung.findUnique({
      where: {
        id: binhLuan.ma_nguoi_binh_luan,
      },
    });
    if (!isValidNguoiBinhLuan) {
      throw new Error('ID nguoi dung khong ton tai');
    }

    binhLuan.ngay_binh_luan = new Date(binhLuan.ngay_binh_luan);
    const data = await this.prisma.binh_luan.update({
      where: {
        id: id,
      },
      data: binhLuan,
    });
    return data;
  }

  // Xoa binh luan
  async deleteBinhLuan(token: string, idParam: string): Promise<BinhLuanDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.binh_luan.delete({
      where: {
        id: id,
      },
    });
    return data;
  }
}
