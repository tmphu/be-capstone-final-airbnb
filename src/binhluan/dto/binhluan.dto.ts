import { ApiProperty } from '@nestjs/swagger/dist';

interface BinhLuanDto {
  id: number;
  ma_phong: number;
  ma_nguoi_binh_luan: number;
  ngay_binh_luan: Date;
  noi_dung: string;
  sao_binh_luan: number;
}

class BinhLuanSwaggerDto {
  @ApiProperty({
    example: 1,
    description: 'ma_phong',
    type: Number,
  })
  ma_phong: number;

  @ApiProperty({
    example: 1,
    description: 'ma_nguoi_binh_luan',
    type: Number,
  })
  ma_nguoi_binh_luan: number;

  @ApiProperty({
    example: '2023-01-01 00:00:00',
    description: 'ngay_binh_luan',
    type: Date,
  })
  ngay_binh_luan: Date;

  @ApiProperty({
    example: 'this is the content',
    description: 'noi_dung',
    type: String,
  })
  noi_dung: string;

  @ApiProperty({
    example: 5,
    description: 'sao_binh_luan',
    type: Number,
  })
  sao_binh_luan: number;
}

export { BinhLuanDto, BinhLuanSwaggerDto };
