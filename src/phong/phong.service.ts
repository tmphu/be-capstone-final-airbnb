import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { PhongDto, PhongSwaggerDto } from './dto/phong.dto';

@Injectable()
export class PhongService {
  constructor(private readonly authService: AuthService) {}
  private prisma = new PrismaClient();

  // Lay toan bo danh sach phong
  async getPhong(): Promise<PhongDto[]> {
    const data = await this.prisma.phong.findMany();
    return data;
  }

  // Lay phong theo id
  async getPhongById(idParam: string): Promise<PhongDto> {
    const id = parseInt(idParam);
    const data = await this.prisma.phong.findUnique({
      where: {
        id: id,
      },
    });
    return data;
  }

  // Lay phong theo vi tri
  async getPhongByViTriId(vi_triParam: string): Promise<PhongDto[]> {
    const vi_tri = parseInt(vi_triParam);
    const data = await this.prisma.phong.findMany({
      where: {
        vi_tri: vi_tri,
      },
    });
    return data;
  }

  // Tao moi phong
  async createPhong(token: string, phong: PhongSwaggerDto): Promise<PhongDto> {
    const isValidToken = await this.authService.validateToken(token);
    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const isValidViTri = await this.prisma.vi_tri.findUnique({
      where: {
        id: phong.vi_tri,
      },
    });
    if (!isValidViTri) {
      throw new Error('Vi tri khong ton tai');
    }

    const data = await this.prisma.phong.create({
      data: phong,
    });
    return data;
  }

  // Cap nhat phong
  async updatePhong(
    token: string,
    idParam: string,
    phong: PhongSwaggerDto,
  ): Promise<PhongDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.phong.update({
      where: {
        id: id,
      },
      data: phong,
    });
    return data;
  }

  // Xoa phong
  async deletePhong(token: string, idParam: string): Promise<PhongDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.phong.delete({
      where: {
        id: id,
      },
    });
    return data;
  }
}
