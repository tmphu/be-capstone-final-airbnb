import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { PhongController } from './phong.controller';
import { PhongService } from './phong.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [PhongController],
  providers: [PhongService, AuthService],
})
export class PhongModule {}
