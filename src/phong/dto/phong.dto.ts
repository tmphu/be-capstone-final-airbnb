import { ApiProperty } from '@nestjs/swagger/dist';

interface PhongDto {
  id: number;
  ten_phong: string;
  khach: number;
  phong_ngu: number;
  giuong: number;
  phong_tam: number;
  mo_ta: string;
  gia_tien: number;
  may_giat: boolean;
  ban_la: boolean;
  tivi: boolean;
  dieu_hoa: boolean;
  wifi: boolean;
  bep: boolean;
  do_xe: boolean;
  ho_boi: boolean;
  ban_ui: boolean;
  hinh_anh: string;
  vi_tri: number;
}

class PhongSwaggerDto {
  @ApiProperty({
    example: 'myAwesomeRoom',
    description: 'ten_phong',
    type: String,
  })
  ten_phong: string;

  @ApiProperty({
    example: 3,
    description: 'khach',
    type: Number,
  })
  khach: number;

  @ApiProperty({
    example: 2,
    description: 'phong_ngu',
    type: Number,
  })
  phong_ngu: number;

  @ApiProperty({
    example: 2,
    description: 'giuong',
    type: Number,
  })
  giuong: number;

  @ApiProperty({
    example: 2,
    description: 'phong_tam',
    type: Number,
  })
  phong_tam: number;

  @ApiProperty({
    example: 'This is a room for rent',
    description: 'mo_ta',
    type: String,
  })
  mo_ta: string;

  @ApiProperty({
    example: 15000,
    description: 'gia_tien',
    type: Number,
  })
  gia_tien: number;

  @ApiProperty({
    example: true,
    description: 'may_giat',
    type: Boolean,
  })
  may_giat: boolean;

  @ApiProperty({
    example: true,
    description: 'ban_la',
    type: Boolean,
  })
  ban_la: boolean;

  @ApiProperty({
    example: true,
    description: 'tivi',
    type: Boolean,
  })
  tivi: boolean;

  @ApiProperty({
    example: true,
    description: 'dieu_hoa',
    type: Boolean,
  })
  dieu_hoa: boolean;

  @ApiProperty({
    example: true,
    description: 'wifi',
    type: Boolean,
  })
  wifi: boolean;

  @ApiProperty({
    example: true,
    description: 'bep',
    type: Boolean,
  })
  bep: boolean;

  @ApiProperty({
    example: true,
    description: 'do_xe',
    type: Boolean,
  })
  do_xe: boolean;

  @ApiProperty({
    example: true,
    description: 'ho_boi',
    type: Boolean,
  })
  ho_boi: boolean;

  @ApiProperty({
    example: true,
    description: 'ban_ui',
    type: Boolean,
  })
  ban_ui: boolean;

  @ApiProperty({
    example: 'https://example.com/image.jpg',
    description: 'hinh_anh',
    type: String,
  })
  hinh_anh: string;

  @ApiProperty({
    example: 3,
    description: 'vi_tri',
    type: Number,
  })
  vi_tri: number;
}

export { PhongDto, PhongSwaggerDto };
