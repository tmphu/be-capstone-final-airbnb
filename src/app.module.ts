import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { BinhluanModule } from './binhluan/binhluan.module';
import { VitriModule } from './vitri/vitri.module';
import { PhongModule } from './phong/phong.module';
import { DatphongModule } from './datphong/datphong.module';
import { NguoidungModule } from './nguoidung/nguoidung.module';

@Module({
  imports: [
    AuthModule,
    ConfigModule.forRoot({ isGlobal: true }),
    BinhluanModule,
    VitriModule,
    PhongModule,
    DatphongModule,
    NguoidungModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
