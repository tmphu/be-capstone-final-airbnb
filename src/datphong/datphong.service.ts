import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { DatPhongDto, DatPhongSwaggerDto } from './dto/datphong.dto';

@Injectable()
export class DatPhongService {
  constructor(private readonly authService: AuthService) {}
  private prisma = new PrismaClient();

  // Lay toan bo danh sach dat phong
  async getDatPhong(): Promise<DatPhongDto[]> {
    const data = await this.prisma.dat_phong.findMany();
    return data;
  }

  // Lay dat phong theo id
  async getDatPhongById(idParam: string): Promise<DatPhongDto> {
    const id = parseInt(idParam);
    const data = await this.prisma.dat_phong.findUnique({
      where: {
        id: id,
      },
    });
    return data;
  }

  // Lay dat phong theo nguoi dung
  async getDatPhongByNguoiDungId(idParam: string): Promise<DatPhongDto[]> {
    const id = parseInt(idParam);
    const data = await this.prisma.dat_phong.findMany({
      where: {
        ma_nguoi_dat: id,
      },
    });
    return data;
  }

  // Tao moi dat phong
  async createDatPhong(
    token: string,
    datPhong: DatPhongSwaggerDto,
  ): Promise<DatPhongDto> {
    const isValidToken = await this.authService.validateToken(token);
    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const isValidPhong = await this.prisma.phong.findUnique({
      where: {
        id: datPhong.ma_phong,
      },
    });
    if (!isValidPhong) {
      throw new Error('ID phong khong ton tai');
    }

    const isValidNguoiDat = await this.prisma.nguoi_dung.findUnique({
      where: {
        id: datPhong.ma_nguoi_dat,
      },
    });
    if (!isValidNguoiDat) {
      throw new Error('ID nguoi dung khong ton tai');
    }

    datPhong.ngay_den = new Date(datPhong.ngay_den);
    datPhong.ngay_di = new Date(datPhong.ngay_di);
    const data = await this.prisma.dat_phong.create({
      data: datPhong,
    });
    return data;
  }

  // Cap nhat dat phong
  async updateDatPhong(
    token: string,
    idParam: string,
    datPhong: DatPhongSwaggerDto,
  ): Promise<DatPhongDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const isValidNguoiDat = await this.prisma.nguoi_dung.findUnique({
      where: {
        id: datPhong.ma_nguoi_dat,
      },
    });
    if (!isValidNguoiDat) {
      throw new Error('ID nguoi dung khong ton tai');
    }

    datPhong.ngay_den = new Date(datPhong.ngay_den);
    datPhong.ngay_di = new Date(datPhong.ngay_di);
    const data = await this.prisma.dat_phong.update({
      where: {
        id: id,
      },
      data: datPhong,
    });
    return data;
  }

  // Xoa dat phong
  async deleteDatPhong(token: string, idParam: string): Promise<DatPhongDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.dat_phong.delete({
      where: {
        id: id,
      },
    });
    return data;
  }
}
