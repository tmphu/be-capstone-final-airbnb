import { ApiProperty } from '@nestjs/swagger/dist';

interface DatPhongDto {
  id: number;
  ma_phong: number;
  ngay_den: Date;
  ngay_di: Date;
  so_luong_khach: number;
  ma_nguoi_dat: number;
}

class DatPhongSwaggerDto {
  @ApiProperty({
    example: 1,
    description: 'ma_phong',
    type: Number,
  })
  ma_phong: number;

  @ApiProperty({
    example: '2023-01-01',
    description: 'ngay_den',
    type: Date,
  })
  ngay_den: Date;

  @ApiProperty({
    example: '2023-01-01',
    description: 'ngay_di',
    type: Date,
  })
  ngay_di: Date;

  @ApiProperty({
    example: 2,
    description: 'so_luong_khach',
    type: Number,
  })
  so_luong_khach: number;

  @ApiProperty({
    example: 1,
    description: 'ma_nguoi_dat',
    type: Number,
  })
  ma_nguoi_dat: number;
}

export { DatPhongDto, DatPhongSwaggerDto };
