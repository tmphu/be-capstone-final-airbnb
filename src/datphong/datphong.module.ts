import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { DatPhongController } from './datphong.controller';
import { DatPhongService } from './datphong.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [DatPhongController],
  providers: [DatPhongService, AuthService],
})
export class DatphongModule {}
