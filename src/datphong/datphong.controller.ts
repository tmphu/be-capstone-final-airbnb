import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  Res,
  Headers,
} from '@nestjs/common';
import { DatPhongService } from './datphong.service';
import { Response } from 'express';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { DatPhongSwaggerDto } from './dto/datphong.dto';
import { Delete, Param, Put } from '@nestjs/common/decorators';

@ApiTags('DatPhong')
@Controller('dat-phong')
export class DatPhongController {
  constructor(private readonly datPhongService: DatPhongService) {}

  // Lay toan bo danh sach dat phong
  @Get('/')
  async getDatPhong(@Res() res: Response): Promise<Response> {
    try {
      const data = await this.datPhongService.getDatPhong();

      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Tao moi dat phong
  @Post('/')
  async createDatPhong(
    @Res() res: Response,
    @Body() body: DatPhongSwaggerDto,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.datPhongService.createDatPhong(token, body);
      return res.status(201).json({
        message: 'Success',
        statusCode: 201,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Lay dat phong theo id
  @Get('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async getDatPhongById(
    @Res() res: Response,
    @Param('id') id: string,
  ): Promise<Response> {
    try {
      const data = await this.datPhongService.getDatPhongById(id);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Lay dat phong theo nguoi dung
  @Get('/lay-theo-nguoi-dung/:nguoiDungId')
  @ApiParam({ name: 'nguoiDungId', required: true, type: Number })
  async getDatPhongByNguoiDungId(
    @Res() res: Response,
    @Param('nguoiDungId') id: string,
  ): Promise<Response> {
    try {
      const data = await this.datPhongService.getDatPhongByNguoiDungId(id);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Cap nhat dat phong
  @Put('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async updateDatPhong(
    @Res() res: Response,
    @Body() body: DatPhongSwaggerDto,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.datPhongService.updateDatPhong(token, id, body);
      return res.status(200).json({
        message: 'Success',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }

  // Xoa dat phong
  @Delete('/:id')
  @ApiParam({ name: 'id', required: true, type: Number })
  async deleteDatPhong(
    @Res() res: Response,
    @Param('id') id: string,
    @Headers('userToken') token: string,
  ): Promise<Response> {
    try {
      const data = await this.datPhongService.deleteDatPhong(token, id);
      return res.status(200).json({
        message: 'Dat phong da duoc xoa thanh cong',
        statusCode: 200,
        content: data,
        dateTime: new Date(),
      });
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.FORBIDDEN);
    }
  }
}
