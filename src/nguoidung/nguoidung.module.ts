import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import { AuthService } from 'src/auth/auth.service';
import { NguoiDungController } from './nguoidung.controller';
import { NguoiDungService } from './nguoidung.service';

@Module({
  imports: [JwtModule.register({})],
  controllers: [NguoiDungController],
  providers: [NguoiDungService, AuthService],
})
export class NguoidungModule {}
