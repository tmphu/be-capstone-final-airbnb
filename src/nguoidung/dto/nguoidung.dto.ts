import { ApiProperty } from '@nestjs/swagger/dist';

interface NguoiDungDto {
  id: number;
  name: string;
  email: string;
  pass_word: string;
  phone: string;
  birth_day: string;
  gender: string;
  role: string;
  hinh_anh: string;
}

class NguoiDungSwaggerDto {
  @ApiProperty({
    example: 'User1',
    description: 'name',
    type: String,
  })
  name: string;

  @ApiProperty({
    example: 'user1@gmail.com',
    description: 'email',
    type: String,
  })
  email: string;

  @ApiProperty({
    example: '123456',
    description: 'pass_word',
    type: String,
  })
  pass_word: string;

  @ApiProperty({
    example: '0945600864',
    description: 'phone',
    type: String,
  })
  phone: string;

  @ApiProperty({
    example: '1990-01-01',
    description: 'birth_day',
    type: String,
  })
  birth_day: string;

  @ApiProperty({
    example: 'male',
    description: 'gender',
    type: String,
  })
  gender: string;

  @ApiProperty({
    example: 'user',
    description: 'role',
    type: String,
  })
  role: string;

  @ApiProperty({
    example: 'https://example.com/image.jpg',
    description: 'hinh_anh',
    type: String,
  })
  hinh_anh: string;
}

export { NguoiDungDto, NguoiDungSwaggerDto };
