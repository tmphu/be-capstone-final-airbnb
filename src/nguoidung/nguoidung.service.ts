import { Injectable } from '@nestjs/common';
import { PrismaClient } from '@prisma/client';
import { AuthService } from 'src/auth/auth.service';
import { NguoiDungDto, NguoiDungSwaggerDto } from './dto/nguoidung.dto';

@Injectable()
export class NguoiDungService {
  constructor(private readonly authService: AuthService) {}
  private prisma = new PrismaClient();

  // Lay toan bo danh sach nguoi dung
  async getNguoiDung(): Promise<NguoiDungDto[]> {
    const data = await this.prisma.nguoi_dung.findMany();
    return data;
  }

  // Lay nguoi dung theo id
  async getNguoiDungById(idParam: string): Promise<NguoiDungDto> {
    const id = parseInt(idParam);
    const data = await this.prisma.nguoi_dung.findUnique({
      where: {
        id: id,
      },
    });
    return data;
  }

  // Search nguoi dung theo ten
  async searchNguoiDungByName(name: string): Promise<NguoiDungDto[]> {
    const data = await this.prisma.nguoi_dung.findMany({
      where: {
        name: {
          contains: name.toLowerCase(),
        },
      },
    });
    return data;
  }

  // Tao moi nguoi dung
  async createNguoiDung(
    token: string,
    nguoiDung: NguoiDungSwaggerDto,
  ): Promise<NguoiDungDto> {
    const isValidToken = await this.authService.validateToken(token);
    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const user = await this.prisma.nguoi_dung.findUnique({
      where: {
        email: nguoiDung.email,
      },
    });
    if (user) {
      throw new Error('Email da ton tai trong he thong');
    }

    const data = await this.prisma.nguoi_dung.create({
      data: nguoiDung,
    });
    return data;
  }

  // Cap nhat nguoi dung
  async updateNguoiDung(
    token: string,
    idParam: string,
    nguoiDung: NguoiDungSwaggerDto,
  ): Promise<NguoiDungDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.nguoi_dung.update({
      where: {
        id: id,
      },
      data: nguoiDung,
    });
    return data;
  }

  // Xoa nguoi dung
  async deleteNguoiDung(token: string, idParam: string): Promise<NguoiDungDto> {
    const id = parseInt(idParam);
    const isValidToken = await this.authService.validateToken(token);

    if (!isValidToken) {
      throw new Error('Token is not valid');
    }

    const data = await this.prisma.nguoi_dung.delete({
      where: {
        id: id,
      },
    });
    return data;
  }
}
