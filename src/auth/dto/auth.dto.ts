import { ApiProperty } from '@nestjs/swagger/dist';

interface UserLoginDto {
  email: string;
  password: string;
}

class UserLoginSwaggerDto {
  @ApiProperty({
    example: 'user@example.com',
    description: 'email',
    type: String,
  })
  email: string;

  @ApiProperty({ example: 'mypassword', description: 'password', type: String })
  password: string;
}

interface UserSignupDto {
  email: string;
  password: string;
  name: string;
  birthDate: string;
  phone: string;
  gender: string;
}

class UserSignupSwaggerDto {
  @ApiProperty({
    example: 'user@example.com',
    description: 'email',
    type: String,
  })
  email: string;

  @ApiProperty({ example: 'mypassword', description: 'password', type: String })
  password: string;

  @ApiProperty({
    example: 'john appleseed',
    description: 'full name',
    type: String,
  })
  name: string;

  @ApiProperty({
    example: '1990-01-01',
    description: 'birthday',
    type: String,
  })
  birthDate: string;

  @ApiProperty({
    example: '091xxxxxxx',
    description: 'phone',
    type: String,
  })
  phone: string;

  @ApiProperty({
    example: 'male',
    description: 'gender',
    type: String,
  })
  gender: string;
}

export {
  UserLoginDto,
  UserSignupDto,
  UserLoginSwaggerDto,
  UserSignupSwaggerDto,
};
