import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaClient } from '@prisma/client';
import { UserSignupDto } from './dto/auth.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private config: ConfigService, private jwt: JwtService) {}
  private prisma = new PrismaClient();
  private readonly logger = new Logger(AuthService.name);

  async login(email: string, password: string): Promise<any> {
    const user = await this.prisma.nguoi_dung.findUnique({
      where: {
        email: email,
      },
    });
    if (user) {
      const isMatch = bcrypt.compareSync(password, (await user).pass_word);
      if (isMatch) {
        const token = this.jwt.sign(
          { data: { email: email, password: password } },
          { secret: this.config.get('SECRET_KEY'), expiresIn: '1d' },
        );
        return token;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  async signup(newUser: UserSignupDto): Promise<any> {
    const user = await this.prisma.nguoi_dung.findUnique({
      where: {
        email: newUser.email,
      },
    });

    if (user != null) {
      return null;
    } else {
      const payload = {
        email: newUser.email,
        pass_word: bcrypt.hashSync(newUser.password, 10),
        name: newUser.name,
        birth_day: newUser.birthDate,
        phone: newUser.phone,
        gender: newUser.gender,
        role: 'user',
      };

      const user = this.prisma.nguoi_dung.create({
        data: payload,
      });

      return user;
    }
  }

  async validateToken(token: string): Promise<boolean> {
    try {
      await this.jwt.verify(token, {
        secret: this.config.get('SECRET_KEY'),
      });
      return true;
    } catch (error) {
      return false;
    }
  }
}
