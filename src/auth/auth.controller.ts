import { Body, Controller, Post, Res } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UserLoginSwaggerDto, UserSignupSwaggerDto } from './dto/auth.dto';
import { Response } from 'express';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('login')
  async login(
    @Body() body: UserLoginSwaggerDto,
    @Res() res: Response,
  ): Promise<any> {
    const { email, password } = body;
    const data = await this.authService.login(email, password);
    if (data) {
      return res.status(201).json({
        message: 'Success',
        statusCode: 200,
        content: data,
      });
    } else {
      return res.status(401).json({
        message: 'Sai tên đăng nhập hoặc mật khẩu',
        statusCode: 401,
        content: null,
      });
    }
  }

  @Post('signup')
  async signup(
    @Body() body: UserSignupSwaggerDto,
    @Res() res: Response,
  ): Promise<any> {
    const data = await this.authService.signup(body);
    if (data == null) {
      return res.status(401).json({
        message: 'Email đã tồn tại trong hệ thống',
        statusCode: 401,
        content: null,
      });
    } else {
      return res.status(201).json({
        message: 'Success',
        statusCode: 201,
        content: data,
      });
    }
  }
}
