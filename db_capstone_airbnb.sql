CREATE DATABASE db_capstone_airbnb;
USE db_capstone_airbnb;

CREATE TABLE nguoi_dung (
	id INT AUTO_INCREMENT,
    `name` VARCHAR(255),
    email VARCHAR(255),
    pass_word VARCHAR(255),
    phone VARCHAR(255),
    birth_day VARCHAR(255),
    gender VARCHAR(255),
    `role` VARCHAR(255),
    hinh_anh VARCHAR(1000),
    PRIMARY KEY (id)
);
ALTER TABLE nguoi_dung ADD CONSTRAINT uc_email UNIQUE (email);

CREATE TABLE vi_tri (
	id INT AUTO_INCREMENT,
    ten_vi_tri VARCHAR(255),
    tinh_thanh VARCHAR(255),
    quoc_gia VARCHAR(255),
    hinh_anh VARCHAR(1000),
    PRIMARY KEY (id)
);

CREATE TABLE phong (
	id INT AUTO_INCREMENT,
    ten_phong VARCHAR(255),
    khach INT,
    phong_ngu INT,
    giuong INT,
    phong_tam INT,
    mo_ta VARCHAR(255),
    gia_tien INT,
    may_giat BOOLEAN,
    ban_la BOOLEAN,
    tivi BOOLEAN,
    dieu_hoa BOOLEAN,
    wifi BOOLEAN,
    bep BOOLEAN,
    do_xe BOOLEAN,
    ho_boi BOOLEAN,
    ban_ui BOOLEAN,
    hinh_anh VARCHAR(1000),
    vi_tri INT,
    PRIMARY KEY (id),
    FOREIGN KEY (vi_tri) REFERENCES vi_tri(id)
);

CREATE TABLE dat_phong (
	id INT AUTO_INCREMENT,
    ma_phong INT,
    ngay_den DATETIME,
    ngay_di DATETIME,
    so_luong_khach INT,
    ma_nguoi_dat INT,
    PRIMARY KEY (id),
    FOREIGN KEY (ma_phong) REFERENCES phong(id),
    FOREIGN KEY (ma_nguoi_dat) REFERENCES nguoi_dung(id)
);

CREATE TABLE binh_luan (
	id INT AUTO_INCREMENT,
    ma_phong INT,
    ma_nguoi_binh_luan INT,
    ngay_binh_luan DATETIME,
    noi_dung VARCHAR(1000),
    sao_binh_luan INT,
    PRIMARY KEY (id),
    FOREIGN KEY (ma_phong) REFERENCES phong(id),
    FOREIGN KEY (ma_nguoi_binh_luan) REFERENCES nguoi_dung(id)
);